package sample;

import com.jfoenix.controls.JFXTextField;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        JFXTextField jfxTextField = new JFXTextField();
        BorderPane root = new BorderPane();
        root.setCenter(jfxTextField);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
        System.out.println(jfxTextField.getLayoutX());
    }


    public static void main(String[] args) {
        launch(args);
    }
}
